import React from 'react';
import { Text, TouchableOpacity, SafeAreaView } from 'react-native';

const JTIButton = ({ onPress, children }) => {
  const { buttonStyle, titleStyle } = styles;
  return (
    <TouchableOpacity onPress={onPress} style={buttonStyle}>
      <SafeAreaView>
        <Text style={titleStyle}> {children}</Text>
      </SafeAreaView>
    </TouchableOpacity>
  );
};

export { JTIButton };

const styles = {
  buttonStyle: {
    flex: 1,
    alignSelf: 'stretch',
    backgroundColor: '#199F7A',
    marginLeft: 0,
    marginRight: 0,
    height: 50,
    justifyContent: 'center'
  },
  titleStyle: {
    alignSelf: 'center',
    color: 'white',
    fontSize: 18,
    fontWeight: '600'
  }
};
