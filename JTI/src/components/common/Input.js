import React from 'react';
import { View, Text, TextInput, Image } from 'react-native';

const Input = ({
  label,
  value,
  onChangeText,
  placeholder,
  secureTextEntry
}) => {
  const { containerStyle, labelStyle, textStyle, logoImageStyle } = Styles;
  return (
    <View style={containerStyle}>
      <Text style={labelStyle}> {label} </Text>
      <TextInput
        style={textStyle}
        placeholder={placeholder}
        value={value}
        onChangeText={onChangeText}
        secureTextEntry={secureTextEntry}
      />
    </View>
  );
};

export { Input };

Styles = {
  containerStyle: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    height: 40,
    paddingRight: 10,
    backgroundColor: '#F8F8F8'
  },
  logoImageStyle: {
    width: 30,
    height: 30
  },
  labelStyle: {
    fontSize: 15,
    color: '#808080',
    paddingLeft: 5
  },
  textStyle: {
    color: '#000',
    paddingRight: 5,
    paddingLeft: 5,
    fontSize: 18,
    flex: 2
  }
};
