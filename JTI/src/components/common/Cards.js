import React from 'react';
import { View, SafeAreaView } from 'react-native';

export const Cards = props => (
  <SafeAreaView>
    <View style={styles.containerStyle}>{props.children}</View>
  </SafeAreaView>
);

const styles = {
  containerStyle: {
    elevation: 1,
    marginLeft: 20,
    marginRight: 20,
    marginTop: 20,
    borderWidth: 1,
    borderColor: 'gray'
  }
};
