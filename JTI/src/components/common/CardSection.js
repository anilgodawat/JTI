import React from 'react';
import { View } from 'react-native';

const CardSection = props => (
  <View style={[styles.ContainerStyle, props.style]}> {props.children} </View>
);

const styles = {
  ContainerStyle: {
    borderBottomWidth: 1,
    padding: 0,
    justifyContent: 'flex-start',
    flexDirection: 'row',
    borderColor: '#ddd',
    position: 'relative'
    }
};

export { CardSection };
