import React, { Component } from 'react';
import { Text, View, Image, TouchableOpacity } from 'react-native';
import { Cards, CardSection } from '../components/common';

const StockRequestItem = ({ onPress, children }) => {
  return (
    <View style={Styles.ContainerStyle}>
      <Cards>
        <View
          style={{ height: 120, backgroundColor: 'white', borderRadius: 5 }}
        >
          <View
            style={{
              flexDirection: 'row',
              flex: 1,
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-between',
              padding: 10
            }}
          >
            <View>
              <Text style={Styles.commonContentStyle}> 01673123545</Text>
              <Text style={Styles.commonContentStyle}> Winston Red Time</Text>
            </View>
            <Image
              style={{ width: 30, height: 20 }}
              source={require('../assets/mail.png')}
            />
          </View>

          <View
            style={{
              flexDirection: 'row',
              flex: 1,
              padding: 10,
              alignItems: 'center',
              justifyContent: 'space-between'
            }}
          >
            <Text> UCM:CS</Text>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                padding: 10
              }}
            >
              <Text style={{ paddingRight: 10 }}>QTY</Text>
              <Image
                style={{ width: 20, height: 20, paddingLeft: 20 }}
                source={require('../assets/mail.png')}
              />

              <Text
                style={{
                  paddingRight: 10,
                  paddingLeft: 10,
                  backgroundColor: 'gray'
                }}
              >
                {' '}
                8
              </Text>
              <Image
                style={{ width: 20, height: 20, paddingLeft: 20 }}
                source={require('../assets/mail.png')}
              />
            </View>
          </View>
        </View>
      </Cards>
    </View>
  );
};

export default StockRequestItem;

const Styles = {
  ContainerStyle: {
    flex: 1
  },
  gridStyle: {
    height: 120,

    borderBottomColor: '#979797',
    borderBottomWidth: 1,
    borderRightColor: '#979797',
    borderRightWidth: 1,
    borderLeftColor: '#979797',
    borderLeftWidth: 1,
    borderTopColor: '#979797',
    borderTopWidth: 1,

    alignItems: 'center',
    justifyContent: 'center'
  },
  titleStyle: {
    color: '#199F7A',
    fontSize: 16
  },
  commonContentStyle: {
    fontSize: 16
  }
};
