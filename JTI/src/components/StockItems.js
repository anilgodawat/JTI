import React, { Component } from 'react';
import { Text, View, Image, TouchableOpacity } from 'react-native';
import { Cards, CardSection } from '../components/common';

const StockItems = ({ onPress, children }) => {
  const { firstMainContainer, mainConinerStyle, titleStyle } = Styles;
  return (
    <TouchableOpacity>
      <View>
        <Cards>
          <CardSection>
            <View style={{ flexDirection: 'row', flex: 1, height: 50 }}>
              <View style={firstMainContainer}>
                <View
                  style={{
                    flexDirection: 'row',
                    padding: 10,
                    flex: 1
                  }}
                >
                  <Text style={titleStyle}> SR-01673 </Text>

                  <Text style={{ fontSize: 17, paddingLeft: 60 }}>
                    Submitted: 12/31/2017
                  </Text>
                  <Text style={{ fontSize: 17, paddingLeft: 30 }}>
                    Approved: N/A
                  </Text>
                </View>
              </View>
              <View style={mainConinerStyle}>
                <View
                  style={{
                    flexDirection: 'row',
                    padding: 10,
                    flex: 1,
                    justifyContent: 'space-between'
                  }}
                >
                  <Text style={{ fontSize: 17, paddingLeft: 40 }}>
                    Approved
                  </Text>
                  <Image
                    style={{ width: 30, height: 20 }}
                    source={require('../assets/mail.png')}
                  />
                  <Image
                    style={{ width: 30, height: 20 }}
                    source={require('../assets/mail.png')}
                  />
                </View>
              </View>
            </View>
          </CardSection>
        </Cards>
      </View>
    </TouchableOpacity>
  );
};

const Styles = {
  firstMainContainer: {
    backgroundColor: 'white',
    height: 50,
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    flexGrow: 4
  },
  mainConinerStyle: {
    backgroundColor: 'white',
    height: 50,
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    flexGrow: 2
  },
  titleStyle: {
    fontSize: 17
  }
};

export default StockItems;
