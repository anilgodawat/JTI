import React from 'react';
import { JTIButton, Cards, Input, CardSection } from '../components/common';
import { Text, View, Image, Button } from 'react-native';

class LoginScreen extends React.Component {
  state = {
    email: '',
    password: '',
    emailError: '',
    passwordError: '',
    isEmailValidated: true,
    isPasswordValidated: true
  };
  onSignInClick() {
    this.props.navigation.navigate('Dashboard');
  }
  onForgotPassword() {
    this.props.navigation.navigate('Inventory');
  }
  render() {
    const {
      containerStyle,
      imageContainer,
      loginContainer,
      footerContainer,
      logoImageStyle,
      touchIdImageStyle
    } = Styles;

    return (
      <View style={containerStyle}>
        <View style={imageContainer}>
          <Image
            style={logoImageStyle}
            source={require('../assets/jti_logo.png')}
          />
        </View>

        <View style={loginContainer}>
          <Cards>
            <CardSection>
              <View
                style={{
                  paddingLeft: 10,
                  justifyContent: 'space-around',
                  backgroundColor: '#F8F8F8'
                }}
              >
                <Image
                  style={{ width: 30, height: 20 }}
                  source={require('../assets/mail.png')}
                />
              </View>
              <Input
                label="Email"
                placeholder=""
                onChangeText={email => this.setState({ email })}
                value={this.state.email}
              />
            </CardSection>
          </Cards>
          <Cards>
            <CardSection>
              <View
                style={{
                  paddingLeft: 10,
                  justifyContent: 'space-around',
                  backgroundColor: '#F8F8F8'
                }}
              >
                <Image
                  style={{ width: 30, height: 20 }}
                  source={require('../assets/stock-sm.png')}
                />
              </View>
              <Input
                label="Password"
                placeholder=""
                onChangeText={email => this.setState({ email })}
                value={this.state.email}
              />
            </CardSection>
          </Cards>
          <Cards>
            <CardSection>
              <JTIButton onPress={this.onSignInClick.bind(this)}>
                Sign In
              </JTIButton>
            </CardSection>
          </Cards>

          <View style={{ paddingTop: 20 }}>
            <Button
              color="gray"
              title="Forgot Password?"
              onPress={this.onForgotPassword.bind(this)}
            />{' '}
          </View>
        </View>
        <View style={footerContainer}>
          <Image
            style={touchIdImageStyle}
            source={require('../assets/touch_icon.png')}
          />
          <Text>Tap to sign in</Text>
        </View>
      </View>
    );
  }
}

export default LoginScreen;

const Styles = {
  headerTitleStyle: {
    fontSize: 20,
    color: 'white'
  },
  containerStyle: {
    flex: 1,
    alignSelf: 'stretch',
    backgroundColor: 'white'
  },
  imageContainer: {
    flex: 2,
    alignItems: 'center',
    justifyContent: 'center'
  },
  logoImageStyle: {
    width: 162,
    height: 123
  },
  loginContainer: {
    flex: 3,
    alignSelf: 'center',
    width: '50%'
  },
  touchIdImageStyle: {
    width: 60,
    height: 60
  },
  footerContainer: {
    flex: 1,
    justifyContent: 'center',
    alignSelf: 'center'
  }
};
