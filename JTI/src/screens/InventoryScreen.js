import React, { Component } from 'react';
import { View, FlatList, Text, TouchableOpacity } from 'react-native';
import { JTIButton, Cards } from '../components/common';
import StockItems from '../components/StockItems';

class InventoryScreen extends Component {
  state = { isStockRequest: true };

  onStockRequestPress() {
    this.setState({ isStockRequest: true });
  }

  onVanInventoryPress() {
    this.setState({ isStockRequest: false });
  }

  onNewRequestClick() {
    console.log('stock cick');
    this.props.navigation.navigate('InventoryDetail');
  }

  renderItem(stockList) {
    return <StockItems stockList={stockList.item} />;
  }

  rednerStockRequest() {
    console.log('new render');
    return (
      <View style={{ backgroundColor: 'red' }}>
        <View
          style={{
            height: 50,
            alignItems: 'flex-end'
          }}
        >
          <View
            style={{
              width: 200,
              flex: 1,
              justifyContent: 'center',
              alignItems: 'center',
              paddingRight: 20
            }}
          >
            <JTIButton> New Request </JTIButton>
          </View>
        </View>
        <FlatList
          data={jsonResponse}
          keyExtractor={response => '' + response.id}
          renderItem={this.renderItem}
        />
      </View>
    );
  }

  render() {
    var jsonResponse = require('../utility/Inventory.json');
    const {
      HeaderStyle,
      headerSelectedStyle,
      headerUnSelectedStyle,
      headerTitle
    } = Styles;
    return (
      <View style={{ flex: 1, backgroundColor: '#F8F8F8' }}>
        <View style={{ height: 90, flexDirection: 'row', padding: 20 }}>
          <View
            style={[
              HeaderStyle,
              this.state.isStockRequest
                ? headerSelectedStyle
                : headerUnSelectedStyle
            ]}
          >
            <TouchableOpacity onPress={this.onStockRequestPress.bind(this)}>
              <Text style={Styles.headerTitle}> STOCK REQUESTS</Text>
            </TouchableOpacity>
          </View>

          <View
            style={[
              HeaderStyle,
              this.state.isStockRequest
                ? headerUnSelectedStyle
                : headerSelectedStyle
            ]}
          >
            <TouchableOpacity onPress={this.onVanInventoryPress.bind(this)}>
              <Text style={Styles.headerTitle}> VAN INVENTORY</Text>
            </TouchableOpacity>
          </View>
        </View>
        <View
          style={{
            height: 50,
            alignItems: 'flex-end'
          }}
        >
          <View
            style={{
              width: 200,
              flex: 1,
              justifyContent: 'center',
              alignItems: 'center',
              paddingRight: 20
            }}
          >
            <JTIButton onPress={this.onNewRequestClick.bind(this)}>
              New Request
            </JTIButton>
          </View>
        </View>
        <FlatList
          data={jsonResponse}
          keyExtractor={response => '' + response.id}
          renderItem={this.renderItem}
        />
      </View>
    );
  }
}

export default InventoryScreen;

const Styles = {
  HeaderStyle: {
    flex: 2,
    justifyContent: 'center',
    alignItems: 'center',
    borderBottomWidth: 3
  },
  headerTitle: {
    fontSize: 18,
    color: 'gray'
  },
  headerSelectedStyle: {
    borderBottomColor: '#199F7A'
  },
  headerUnSelectedStyle: {
    borderBottomColor: 'gray'
  }
};
