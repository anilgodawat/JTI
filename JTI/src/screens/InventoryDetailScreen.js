import React, { Component } from 'react';
import { JTIButton, Cards, Input, CardSection } from '../components/common';
import { View, FlatList } from 'react-native';
import StockRequestItem from '../components/StockRequestItem';

class InventoryDetailScreen extends Component {
  renderItem(stockList) {
    return <StockRequestItem stockList={stockList.item} />;
  }
  render() {
    var jsonResponse = require('../utility/Inventory.json');
    return (
      <View style={{ flex: 1, backgroundColor: '#F8F8F8' }}>
        <FlatList
          numColumns="2"
          data={jsonResponse}
          keyExtractor={response => '' + response.id}
          renderItem={this.renderItem}
        />
      </View>
    );
  }
}

export default InventoryDetailScreen;
