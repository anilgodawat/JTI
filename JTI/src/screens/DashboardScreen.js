import React from 'react';
import { Text, View, Image } from 'react-native';

export default class DashbaordScreen extends React.Component {
  render() {
    const { ContainerStyle, gridStyle, titleStyle } = Styles;
    return (
      <View style={ContainerStyle}>
        <View style={gridStyle}>
          <Image
            style={{ width: 50, height: 46 }}
            source={require('../assets/users-sm.png')}
          />
          <Text style={titleStyle}> CUSTOMERS</Text>
        </View>

        <View style={gridStyle}>
          <Image
            style={{ width: 54, height: 54 }}
            source={require('../assets/sync-sm.png')}
          />
          <Text style={titleStyle}> SYNC</Text>
        </View>

        <View style={gridStyle}>
          <Image
            style={{ width: 60, height: 42 }}
            source={require('../assets/mail.png')}
          />
          <Text style={titleStyle}> MESSAGING</Text>
        </View>
        <View style={gridStyle}>
          <Image
            style={{ width: 60, height: 60 }}
            source={require('../assets/products-sm.png')}
          />
          <Text style={titleStyle}> PRODUCTS</Text>
        </View>
        <View style={gridStyle}>
          <Image
            style={{ width: 56, height: 56 }}
            source={require('../assets/reports-sm.png')}
          />
          <Text style={titleStyle}> DAY END REPORT</Text>
        </View>
        <View style={gridStyle}>
          <Image
            style={{ width: 54, height: 54 }}
            source={require('../assets/money-sm.png')}
          />
          <Text style={titleStyle}> SUMMARY</Text>
        </View>
        <View style={gridStyle}>
          <Image
            style={{ width: 60, height: 38 }}
            source={require('../assets/stock-sm.png')}
          />
          <Text style={titleStyle}> STOCK/INVENTORY</Text>
        </View>

        <View style={gridStyle}>
          <Image
            style={{ width: 48, height: 50 }}
            source={require('../assets/performance-sm.png')}
          />
          <Text style={titleStyle}> PERFORMANCE</Text>
        </View>

        <View style={gridStyle}>
          <Image
            style={{ width: 54, height: 54 }}
            source={require('../assets/admin-sm.png')}
          />
          <Text style={titleStyle}> ADMIN</Text>
        </View>
      </View>
    );
  }
}

const Styles = {
  ContainerStyle: {
    flexDirection: 'row',
    flex: 1,
    flexWrap: 'wrap',
    backgroundColor: 'white'
  },
  gridStyle: {
    height: 170,
    width: '25%',
    borderBottomColor: '#979797',
    borderBottomWidth: 1,
    borderRightColor: '#979797',
    borderRightWidth: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  titleStyle: {
    color: '#199F7A',
    fontSize: 16,
    paddingTop: 20
  }
};
