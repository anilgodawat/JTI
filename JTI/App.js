import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { createStackNavigator, createSwitchNavigator } from 'react-navigation';

import LoginScreen from './src/screens/LoginScreen';
import DashboardScreen from './src/screens/DashboardScreen';
import InventoryScreen from './src/screens/InventoryScreen';

import InventoryDetailScreen from './src/screens/InventoryDetailScreen';

export default class App extends React.Component {
  render() {
    const Layout = createRootNavigator(SingedOutStack);
    return <Layout />;
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center'
  }
});

const SingedOutStack = createStackNavigator({
  Login: {
    screen: LoginScreen,
    headerMode: null,
    navigationOptions: ({ navigation }) => ({
      headerTintColor: 'black',
      title: '',
      headerStyle: { backgroundColor: '#FFFFFF', borderBottomColor: '#ffffff' }
    })
  }
});

const SignedInStack = createStackNavigator({
  Dashboard: {
    screen: DashboardScreen,
    headerMode: 'float',
    navigationOptions: ({ navigation }) => ({
      headerTintColor: 'white',
      title: 'DASHBOARD',
      headerStyle: {
        backgroundColor: '#199F7A',
        borderBottomColor: '#ffffff'
      }
    })
  },
  Inventory: {
    screen: InventoryScreen,
    headerMode: 'float',
    navigationOptions: ({ navigation }) => ({
      headerTintColor: 'white',
      title: 'STOCK/INVENTORY',
      headerStyle: {
        backgroundColor: '#199F7A',
        borderBottomColor: '#ffffff'
      }
    })
  },
  InventoryDetail: {
    screen: InventoryDetailScreen,
    headerMode: 'float',
    navigationOptions: ({ navigation }) => ({
      headerTintColor: 'white',
      title: 'STOCK REQUEST',
      headerStyle: {
        backgroundColor: '#199F7A',
        borderBottomColor: '#ffffff'
      }
    })
  }
});

export const createRootNavigator = () => {
  return createSwitchNavigator(
    {
      SingedOutStack: {
        screen: SingedOutStack
      },
      SignedInStack: {
        screen: SignedInStack
      }
    },
    {
      //initialRouteName: signedIn ? "SignedIn" : "SignedOut"
      initialRouteName: SignedInStack ? 'SingedOutStack' : 'SignedInStack'
    }
  );
};
